#!/bin/bash -ux

SCRIPT_FOLDER=`dirname $(readlink -f $0)`
REPO_ROOT="${SCRIPT_FOLDER}"

if [ ! -z $CI_COMMIT_TAG ]
then
  set +x # do not show FTP_* variable on command line
  curl -T "${PD_NDI_ARCHIVE}" ftp://${FTP_USER}:${FTP_PASSWORD}@${FTP_URL}/releases/
fi