#pragma once

#include "m_pd.h"
#include "Gem/Gem/State.h"
#include "Gem/Base/GemBase.h"
#include "Gem/Base/GemPixObj.h"

#include <Processing.NDI.Lib.h>

#include <thread>
#include <mutex>

class GEM_EXTERN ndi_input : public GemBase
{
  CPPEXTERN_HEADER(ndi_input, GemBase)

public:
  ndi_input();
  ~ndi_input();

  void source_mess_cb(t_symbol*s, int argc, t_atom* argv);
  void enumerate_mess_cb();

protected:
  void render(GemState *state);
  void stopRendering();
  void postrender(GemState *);

private:
  void stop_capture();
  static NDIlib_find_instance_t s_ndi_finder;
  static std::thread s_discovery_thread;
  static uint32_t s_no_sources;
  static const NDIlib_source_t* s_sources;
  static bool s_quit;
  static unsigned int s_count;
  static std::mutex s_source_mutex;

  std::string m_pattern{};
  NDIlib_recv_instance_t m_ndi_recv{};
  NDIlib_video_frame_v2_t m_video_frame{};

  std::thread m_capture_thread{};
  bool m_stop{};
  bool m_new_frame{};
  pixBlock m_front_frame{};
  std::mutex m_frame_mutex{};

  t_outlet* m_outlet;
};
