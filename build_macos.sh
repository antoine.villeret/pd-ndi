#!/bin/bash -ex

SCRIPT_FOLDER=`dirname $(greadlink -f $0)`
REPO_ROOT="${SCRIPT_FOLDER}"
BUILD_FOLDER="${REPO_ROOT}/build-macos"

function cleanup {
  if [ -d  ${REPO_ROOT}/3rdparty/NDI_SDK_for_Apple ]; then
    echo "Removing extracted files"
    rm -rf ${REPO_ROOT}/3rdparty/NDI_SDK_for_Apple 
  fi
}

trap cleanup EXIT

if [ -z $CI_COMMIT_TAG ]
then
  TIMESTAMP=`date +%Y.%m.%d-%H.%M.%S`
  HASH=`git describe --always || echo NO_HASH`
  VERSION=${TIMESTAMP}-${HASH}
else
  VERSION=$CI_COMMIT_TAG
fi
echo "Building Version: ${VERSION}"

PD_NDI_ARCHIVE=${BUILD_FOLDER}/package/pd-ndi-${VERSION}-macos.zip

pushd ${REPO_ROOT}/3rdparty
if [ ! -d NDI_SDK_for_Apple ]; then
  unzip -o NDI_SDK_v4_for_Apple.zip > /dev/null
fi
popd

rm -rf ${BUILD_FOLDER}
mkdir -p ${BUILD_FOLDER} && cd ${BUILD_FOLDER}
cmake .. -GNinja \
	-DPUREDATA_INCLUDE_DIRS=/Applications/Pd-0.50-2.app/Contents/Resources/src/ \
	-DGEM_INCLUDE_DIRS=${HOME}/Documents/Pd/externals/Gem/include/ \
	-DCMAKE_INSTALL_PREFIX=${PWD}/install

cmake --build .
cmake --build . --target install
cd package && tar cf ${PD_NDI_ARCHIVE} *

if [ ! -z $CI_COMMIT_TAG ]
then
  set +x # do not show FTP_* variable on command line
  curl -T "${PD_NDI_ARCHIVE}" ftp://${FTP_USER}:${FTP_PASSWORD}@${FTP_URL}/releases/
fi