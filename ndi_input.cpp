#include "ndi_input.hpp"

#include <iostream>
#include <sstream>

#include <wildcards.hpp>

CPPEXTERN_NEW(ndi_input);

NDIlib_find_instance_t ndi_input::s_ndi_finder = nullptr;
std::thread ndi_input::s_discovery_thread = {};
uint32_t ndi_input::s_no_sources = 0;
const NDIlib_source_t* ndi_input::s_sources = nullptr;
bool ndi_input::s_quit = false;
unsigned int ndi_input::s_count = 0;
std::mutex ndi_input::s_source_mutex = {};

ndi_input::ndi_input()
{
  s_count++;

  if(s_count == 1)
  {
    s_discovery_thread = std::thread([&]()
    {
      s_ndi_finder = NDIlib_find_create_v2();
      s_quit = false;

      s_no_sources = 0;
      while (!s_quit)
      {	// Wait until the sources on the nwtork have changed
        NDIlib_find_wait_for_sources(s_ndi_finder, 1000/* One second */);
        s_source_mutex.lock();
        s_sources = NDIlib_find_get_current_sources(s_ndi_finder, &s_no_sources);
        s_source_mutex.unlock();
      }

      NDIlib_find_destroy(s_ndi_finder);
    });
  }

  m_outlet = outlet_new(x_obj, nullptr);
}

ndi_input::~ndi_input()
{
  s_count--;
  if(s_count == 0)
  {
    s_quit = true;
    s_discovery_thread.detach();
  }
}
void ndi_input::render(GemState *state)
{
  if(!m_ndi_recv)
  {
    s_source_mutex.lock();
    for(unsigned int i = 0; i < s_no_sources && !m_ndi_recv; i++)
    {
      std::string src_name(s_sources[i].p_ndi_name);

      if(src_name == m_pattern || wildcards::match(src_name, m_pattern))
      {        
        m_ndi_recv = NDIlib_recv_create_v3();
        NDIlib_recv_connect(m_ndi_recv, &s_sources[i]);

        m_capture_thread = std::thread([&](){
          NDIlib_video_frame_v2_t video_frame;
          imageStruct m_back_frame{};
          m_stop = false;

          while(!m_stop)
          {
            bool has_error = false;
            switch (NDIlib_recv_capture_v2(m_ndi_recv, &video_frame, nullptr, nullptr, 200))
            {
              // Video data
              case NDIlib_frame_type_video:
                m_back_frame.upsidedown = true;
                m_back_frame.xsize = video_frame.xres;
                m_back_frame.ysize = video_frame.yres;
                m_back_frame.setCsizeByFormat(GEM_RGBA);
                switch(video_frame.FourCC)
                {
                  case NDIlib_FourCC_video_type_RGBA:
                  case NDIlib_FourCC_video_type_RGBX:
                    m_back_frame.fromRGBA(video_frame.p_data);
                    break;
                  case NDIlib_FourCC_video_type_BGRA:
                  case NDIlib_FourCC_video_type_BGRX:
                    m_back_frame.fromBGRA(video_frame.p_data);
                    break;
                  case NDIlib_FourCC_video_type_UYVY:
                  case NDIlib_FourCC_video_type_UYVA:
                    m_back_frame.fromUYVY(video_frame.p_data);
                    break;
                  case NDIlib_FourCC_video_type_YV12:
                  case NDIlib_FourCC_video_type_I420:
                    m_back_frame.fromYV12(video_frame.p_data);
                    break;
                  default:
                    has_error = true;
                    std::cerr << "FourCC not supported " << video_frame.FourCC << std::endl;
                }

                if(!has_error)
                {
                  m_frame_mutex.lock();
                  m_back_frame.copy2Image(&m_front_frame.image);
                  m_front_frame.newimage=true;
                  // m_front_frame.image.upsidedown=false;
                  m_frame_mutex.unlock();
                }

                NDIlib_recv_free_video_v2(m_ndi_recv, &video_frame);

                break;
              default:
                ;
            }
          }

          NDIlib_recv_destroy(m_ndi_recv);
          m_ndi_recv = nullptr;
        });
      }
    }
    s_source_mutex.unlock();
  }

  // post("got frame: %p", &m_front_frame);
  m_frame_mutex.lock();
  state->set(GemState::_PIX, &m_front_frame);
}

void ndi_input::stopRendering()
{
  m_stop = true;
  if(m_capture_thread.joinable())
    m_capture_thread.detach();
}

void ndi_input::postrender(GemState *)
{
    m_frame_mutex.unlock();
}

void ndi_input::source_mess_cb(t_symbol*s, int argc, t_atom* argv)
{
  if(argc == 1 && argv->a_type == A_SYMBOL)
  {
    m_pattern = std::string(argv->a_w.w_symbol->s_name);
  }
  else if(argc == 2 && argv[0].a_type == A_SYMBOL && argv[1].a_type == A_SYMBOL)
  {
    std::stringstream ss;
    ss << std::string(argv[0].a_w.w_symbol->s_name)
       << " \\(" << std::string(argv[1].a_w.w_symbol->s_name) << "\\)";
    m_pattern = ss.str();
    stop_capture();
  }
}

void ndi_input::enumerate_mess_cb()
{
  s_source_mutex.lock();
  t_atom items[2];
  SETFLOAT(items, s_no_sources);
  outlet_anything(m_outlet, gensym("count"), 1, items);

  for(int i = 0; i<s_no_sources; i++)
  {
    std::string str(s_sources->p_ndi_name);

    std::size_t separator = str.find_first_of(" ");
    std::string host = str.substr(0, separator);
    std::string name = str.substr(separator + 2, str.size() - (separator + 3));

    SETSYMBOL(items, gensym(host.c_str()));
    SETSYMBOL(items+1, gensym(name.c_str()));
    outlet_anything(m_outlet, gensym("source"), 2, items);
  }
  s_source_mutex.unlock();
}

void ndi_input::stop_capture()
{
  m_stop = true;
  if(m_capture_thread.joinable())
    m_capture_thread.detach();
}

void ndi_input::obj_setupCallback(t_class *classPtr)
{
  CPPEXTERN_MSG (classPtr, "source", source_mess_cb);
  CPPEXTERN_MSG0(classPtr, "enumerate", enumerate_mess_cb);

  NDIlib_initialize();
}

